'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EventSchema = new Schema({
  name: String,
  title: String,
  description: String,
  nickname: String,
  website: String,
  place: [{ location: String, date: Date, start: Date, end: Date }],
  subcats: [{ type: Schema.ObjectId, ref: 'Subcat' }],
  weights: [Number],
  answers: {},
  tags: [String],
  subcatTags: [String],
  answerTags: [String],
  cost: { min: Number, max: Number },
  attendance: Number,
  pets: String,
  kids: String,
  age: Number,
  feel: { fun: Number, classy: Number },
  rsvp: { required: Boolean, start: Date, end: Date },
  source: { type: Schema.ObjectId, ref: 'Source' },
  complete: Boolean,
  sourcedAt: Date
});

var preHook = function (next) {
  var theEvent = this;
  var Question = mongoose.model('Question');
  var qIds = [];

  console.log('description');
  if (theEvent.description !== null && theEvent.description.length > 0) {
    this.complete = true;
    console.log(this);
  }
  console.log('sourcedAt');
  if (typeof theEvent.sourcedAt === 'undefined' || theEvent.sourcedAt === null) {
    this.sourcedAt = new Date();
    console.log(this);
  }

  for (var q in theEvent.answers) {
    qIds.push(q);
  }
  Question.find().in('_id', qIds).exec(function(err, questions) {
    var aset = {};
    for (var qi=0; qi < questions.length; qi++) {
      aset[questions[qi]._id] = {};
      for (var ai=0; ai < questions[qi].answers.length; ai++) {
        aset[questions[qi]._id][questions[qi].answers[ai]._id] = questions[qi].answers[ai];
      }
    }
    for (var q in theEvent.answers) {
      var theAns = aset[q][theEvent.answers[q]];
      if (typeof theAns !== 'undefined' && theAns !== null) {
        for (var j=0; j < theAns.tags.length; j++) {
          if (theEvent.tags.indexOf(theAns.tags[j]) === -1) {
            theEvent.tags.push(theAns.tags[j]);
          }
        }
      }
    }
    next();
  });
};
EventSchema.pre('save', preHook);
EventSchema.pre('update', preHook);

module.exports = mongoose.model('Event', EventSchema);
