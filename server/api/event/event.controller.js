'use strict';

var _ = require('lodash');
var Event = require('./event.model'),
  Subcat = require('../subcat/subcat.model'),
  Question = require('../question/question.model'),
  mongoose = require('mongoose');

var update_tags = function(event) {
  var subcats = [],
    subcatTags = [];
  Subcat.find({ _id: { $in: event.subcats }}, function (err, docs) {
    _(docs).forEach(function(doc) {
      _(doc.tags).forEach(function(tag) {
        subcatTags.push(tag);
      });
    });
    Event.findByIdAndUpdate(event._id, { $set: { subcatTags: subcatTags } }, function (err, docs) {
    });
  });

  var answers = [],
    answerTags = [];
  _.forOwn(event.answers, function(value, key) {
    if (Array.isArray(value)) {
      _(value).forEach(function(ans) {
        if (ans !== null) {
          answers.push(mongoose.Types.ObjectId(ans));
        }
      });
    } else {
      if (value !== null) {
        try {
          answers.push(mongoose.Types.ObjectId(value));
        } catch(e) {
          answerTags.push(value);
        }
      }
    }
  });
  answers = _.reject(answers, function(val) { return answerTags.indexOf(val) > -1 });
  var answer_ids = _.map(answers, function(val) { return val.toString(); });
  Question.aggregate([
    { $unwind: "$answers" },
    { $project : { ansid: "$answers._id", tags: "$answers.tags" } },
    { $match: { ansid: { $in: _.union(answers, answer_ids) } } }
  ], function (err, docs) {
    if (docs.length > 0) {
      _(docs).forEach(function(doc) {
        var ans_i = _.findIndex(answers, function(id) {
          return id.toString() === doc.ansid.toString();
        });
        if (ans_i > -1) {
          answers.splice(ans_i, 1);
          _(doc.tags).forEach(function(tag) {
            answerTags.push(tag);
          });
        }
      });
      _(answers).forEach(function(tag) {
        answerTags.push(tag);
      });
    }
    Event.findByIdAndUpdate(event._id, { $set: { answerTags: answerTags } }, function (err, docs) {
    });
  });
};

// Get list of events
exports.index = function(req, res) {
  var result_func = function (err, events) {
    if(err) { return handleError(res, err); }
    return res.json(200, events);
  };
  if (typeof req.query.s !== 'undefined') {
    var re = new RegExp(req.query.s, 'i');
    // TODO: Add tags
    return Event.find().or([ { name: re }, { title: re }, { nickname: re } ]).exec(result_func);
  } else if (typeof req.query.type !== 'undefined' && req.query.type === 'uncurated') {
    return Event.find({ $or: [ { answers: null }, { answers: { $exists: false } } ] }, result_func);
  } else {
    return Event.find(req.query, result_func);
  }
};

// Get a single event
exports.show = function(req, res) {
  Event.findById(req.params.id, function (err, event) {
    if(err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    return res.json(event);
  });
};

// Creates a new event in the DB.
exports.create = function(req, res) {
  Event.create(req.body, function(err, event) {
    if(err) { return handleError(res, err); }
    return res.json(201, event);
  });
};

// Updates an existing event in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Event.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err, event) {
    if (err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    update_tags(event);
    return res.json(200, event);
  });
};

// Deletes a event from the DB.
exports.destroy = function(req, res) {
  Event.findById(req.params.id, function (err, event) {
    if(err) { return handleError(res, err); }
    if(!event) { return res.send(404); }
    event.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.run = function(req, res) {
  var fs = require('fs'),
    XLSX = require('xlsx'),
    Subcat = mongoose.model('Subcat');

  Subcat.find({}, function (err, subcats) {
    if (!err) {
      var subcatsByCode = {};
      for (var i=0; i < subcats.length; i++) {
        subcatsByCode[subcats[i].code] = subcats[i];
      }
      var workbook = XLSX.readFile(__dirname+'/../../export_events.xlsx');
      var sheet = workbook.Sheets.Events;
      // A:name	B:link	C:description	D:type	E:notes	F:archetype	G:tags	H:1 or 2 days?	I:FAKE DATE 1	J:FAKE DATE 2	K:FAKE START TIME	L:Duration	M:FAKE END TIME	N:FAKE LOCATION IN AUSTIN	O:FAKE COST	P:FAKE KIDS	Q:FAKE DOGS	R:Location	S:subcat_code_1	T:amount_1	U:subcat_code_2	V:amount_2	W:subcat_code_3	X:amount_3

      var cellValue = function(sheet, col, row) {
        var addr = '' + col + row;
        if (typeof sheet[addr] !== 'undefined') {
          return sheet[addr].v;
        }
        return null;
      };

      for (i=2; i < 992; i++) {
        var event = new Event({
          name: cellValue(sheet, 'A', i),
          description: cellValue(sheet, 'C', i),
          nickname: '',
          website: cellValue(sheet, 'B', i),
          cost: { min: cellValue(sheet, 'O', i), max: cellValue(sheet, 'O', i) },
          place: [],
          subcats: [],
          tags: ['imported'],
          weights: [],
        });
        event.title = event.description.substring(0, 140);
        var pets = cellValue(sheet, 'Q', i);
        if (pets === 'No dogs') {
          event.pets = 'no_pets';
        } else if (pets === 'For Dogs') {
          event.pets = 'for_pets';
        } else {
          event.pets = 'pet_friendly';
        }
        var kids = cellValue(sheet, 'P', i);
        if (kids === 'No kids') {
          event.kids = 'no_kids';
        } else if (kids === 'For Kids') {
          event.kids = 'for_kids';
        } else {
          event.kids = 'kid_friendly';
        }
        var date = new Date(sheet['I'+i].w);
        var start = new Date(cellValue(sheet, 'K', i));
        var end = new Date(start.getTime() + (cellValue(sheet, 'L', i) * (60*60*1000) ));
        event.place.push({
          location: cellValue(sheet, 'N', i),
          date: date,
          start: start,
          end: end,
        });
        if (cellValue(sheet, 'H', i) === 2) {
          date = new Date(sheet['J'+i].w);
          event.place.push({
            location: cellValue(sheet, 'N', i),
            date: date,
            start: start,
            end: end,
          });
        }
        var subcat = cellValue(sheet, 'S', i);
        if (subcat !== null && subcatsByCode[subcat] !== null && typeof subcatsByCode[subcat] !== 'undefined') {
          event.subcats.push(subcatsByCode[subcat]);
          event.weights.push(cellValue(sheet, 'T', i));
        }
        subcat = cellValue(sheet, 'U', i);
        if (subcat !== null && subcatsByCode[subcat] !== null && typeof subcatsByCode[subcat] !== 'undefined') {
          event.subcats.push(subcatsByCode[subcat]);
          event.weights.push(cellValue(sheet, 'V', i));
        }
        subcat = cellValue(sheet, 'W', i);
        if (subcat !== null && subcatsByCode[subcat] !== null && typeof subcatsByCode[subcat] !== 'undefined') {
          event.subcats.push(subcatsByCode[subcat]);
          event.weights.push(cellValue(sheet, 'X', i));
        }
        var tag_val = cellValue(sheet, 'G', i);
        if (typeof tag_val !== 'undefined' && tag_val !== null) {
          var tags = tag_val.split(';');
          for (var ti=0; ti < tags.length; ti++) {
            event.tags.push(tags[ti].trim());
          }
        }
        event.save();
      }
      res.jsonp(1);
    } else {
      return res.send(err);
    }
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
