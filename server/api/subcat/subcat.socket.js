/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Subcat = require('./subcat.model');

exports.register = function(socket) {
  Subcat.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Subcat.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('subcat:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('subcat:remove', doc);
}