'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SubcatSchema = new Schema({
  name: String,
  category: String,
  code: String,
  tags: [String],
  questions: [{ type: Schema.ObjectId, ref: 'Question' }],
  eventCount: Number
});

module.exports = mongoose.model('Subcat', SubcatSchema);
