'use strict';

var _ = require('lodash');
var Subcat = require('./subcat.model'),
  Event = require('../event/event.model');

// Get list of subcats
exports.index = function(req, res) {
  Subcat.find(req.query, function (err, subcats) {
    if(err) { return handleError(res, err); }

    var subIds = _.map(subcats, function(n) { return n._id; });
    Event.aggregate([
      { $unwind: "$subcats" },
      { $project : { subid: "$subcats" } },
      { $match: { subid: { $in: subIds } } },
      { $group: { '_id' : '$subid', number : { $sum : 1 } } }
    ], function (err, counts) {
      if(err) { return handleError(res, err); }

      var subcatCounts = {};
      _.each(counts, function(n) { subcatCounts[n._id] = n.number; });
      _.each(subcats, function(n) { n.eventCount = subcatCounts[n._id]; });
      return res.json(200, subcats);
    });
  });
};

// Get a single subcat
exports.show = function(req, res) {
  Subcat.findById(req.params.id, function (err, subcat) {
    if(err) { return handleError(res, err); }
    if(!subcat) { return res.send(404); }
    return res.json(subcat);
  });
};

// Creates a new subcat in the DB.
exports.create = function(req, res) {
  Subcat.create(req.body, function(err, subcat) {
    if(err) { return handleError(res, err); }
    return res.json(201, subcat);
  });
};

// Updates an existing subcat in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Subcat.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err, subcat) {
    if (err) { return handleError(res, err); }
    if(!subcat) { return res.send(404); }
    return res.json(200, subcat);
  });
};

// Deletes a subcat from the DB.
exports.destroy = function(req, res) {
  Subcat.findById(req.params.id, function (err, subcat) {
    if(err) { return handleError(res, err); }
    if(!subcat) { return res.send(404); }
    subcat.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
