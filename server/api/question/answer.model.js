'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AnswerSchema = new Schema({
  name: String,
  tags: [String]
});

module.exports = AnswerSchema;
