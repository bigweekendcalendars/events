'use strict';

var _ = require('lodash');
var Question = require('./question.model'),
  Subcat = require('../subcat/subcat.model');

var add_question_to_subcat = function(q_id, sub_id) {
  Subcat.findById(sub_id, function (err, subcat) {
    if (!err && subcat !== null) {
      if (subcat.questions.indexOf(q_id) === -1) {
        subcat.questions.push(q_id);
        subcat.save();
      }
    }
  });
};

var remove_question_from_subcat = function(q_id, sub_id) {
  Subcat.findById(sub_id, function (err, subcat) {
    if (!err) {
      subcat.questions.splice(subcat.questions.indexOf(q_id), 1);
      subcat.save();
    }
  });
};

var update_related_subcats = function(q_id, selected_subcats) {
  Subcat.find({ questions: { $in: [q_id] } }, function (err, all_subcats) {
    if (!err) {
      for (var i in all_subcats) {
        if (selected_subcats.indexOf(all_subcats[i]._id + '') === -1) {
          remove_question_from_subcat(q_id, all_subcats[i]._id);
        }
      }
      for (i in selected_subcats) {
        add_question_to_subcat(q_id, selected_subcats[i]);
      }
    }
  });
};

// Get list of questions
exports.index = function(req, res) {
  Question.find(req.query, function (err, questions) {
    if(err) { return handleError(res, err); }
    return res.json(200, questions);
  });
};

// Get a single question
exports.show = function(req, res) {
  Question.findById(req.params.id, function (err, question) {
    if(err) { return handleError(res, err); }
    if(!question) { return res.send(404); }
    return res.json(question);
  });
};

// Creates a new question in the DB.
exports.create = function(req, res) {
  var subcats = req.body.subcats;
  delete req.body.subcats;
  Question.create(req.body, function(err, question) {
    if(err) { return handleError(res, err); }
    update_related_subcats(question._id, subcats);
    return res.json(201, question);
  });
};

// Updates an existing question in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  var subcats = req.body.subcats;
  delete req.body.subcats;
  Question.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err, question) {
    if (err) { return handleError(res, err); }
    if(!question) { return res.send(404); }
    update_related_subcats(question._id, subcats);
    return res.json(200, question);
  });
};

// Deletes a question from the DB.
exports.destroy = function(req, res) {
  Question.findById(req.params.id, function (err, question) {
    if(err) { return handleError(res, err); }
    if(!question) { return res.send(404); }
    question.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
