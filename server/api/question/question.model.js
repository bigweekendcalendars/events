'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    AnswerSchema = require('./answer.model');

var QuestionSchema = new Schema({
  name: String,
  questionType: String,
  answers: [AnswerSchema]
});

module.exports = mongoose.model('Question', QuestionSchema);
