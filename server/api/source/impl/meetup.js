'use strict';

var _ = require('lodash'),
  source = require('../source.model'),
  Event = require('../../event/event.model'),
  Promise = require('bluebird');

var retrieveEvents = function(source) {
  var meetup = require('meetup-api')({
    key: source.config.key
  });
  var meetup_opts = {
    zip: source.config.zip,
    radius: source.config.radius,
    category: source.config.categories
  };
  var getOpenEvents = Promise.promisify(meetup.getOpenEvents);
  return getOpenEvents(meetup_opts);
};

var processEvent = function(event) {
  Event.find({ tags: 'meetup-' + event.id}).select('_id').exec()
    .then(function(events) {
      if (events.length > 0) {
        return updateEvent(events[0]._id, event);
      } else {
        return createEvent(event);
      }
    });
};

var eventData = function(event) {
  var stdate = new Date(0);
  var eddate = new Date(0);
  stdate.setUTCSeconds(event.time);
  eddate.setUTCSeconds(event.time + event.duration);
  var evData = {
    name: event.name,
    title: event.group.name,
    description: event.description,
    website: event.event_url,
    place: [{
      date: stdate,
      start: stdate,
      end: eddate
    }],
    tags: ['meetup','tech','meetup-' + event.id]
  };
  if (event.venue !== undefined) {
    evData.place[0].location = event.venue.address_1;
  }
  return evData;
};

var updateEvent = function(id, event) {
  return Event.findByIdAndUpdate(id, eventData(event));
}

var createEvent = function(event) {
  return Event.create(eventData(event));
};

var processEvents = function(events) {
  return Promise.all(events.results.map(processEvent));
};

exports.process = function(source, callback) {
  return retrieveEvents(source)
    .then(processEvents);
};
