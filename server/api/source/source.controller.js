'use strict';

var _ = require('lodash');
var Source = require('./source.model'),
  Event = require('../event/event.model');

// Get list of sources
exports.index = function(req, res) {
  Source.find(req.query, function (err, sources) {
    if(err) { return handleError(res, err); }
    return res.json(200, sources);
  });
};

// Get a single source
exports.show = function(req, res) {
  Source.findById(req.params.id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.send(404); }
    return res.json(source);
  });
};

// Creates a new source in the DB.
exports.create = function(req, res) {
  Source.create(req.body, function(err, source) {
    if(err) { return handleError(res, err); }
    return res.json(201, source);
  });
};

// Updates an existing source in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.send(404); }
    return res.json(200, source);
  });
};

// Deletes a source from the DB.
exports.destroy = function(req, res) {
  Source.findById(req.params.id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.send(404); }
    source.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

exports.run = function(req, res) {
  Source.findById(req.params.id).exec()
    .then(function(source) {
      var impl = require('./impl/' + source.sourceType);
      return impl.process(source);
    }).then(function(result) {
      return res.json(result);
    }, function(err) {
      handleError(res, err);
    });
};

function handleError(res, err) {
  console.error(err);
  console.trace(err);
  return res.send(500, err);
}
