'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SourceSchema = new Schema({
  name: String,
  sourceType: String,
  config: Schema.Types.Mixed,
  lastRun: Date,
  lastResult: Schema.Types.Mixed,
  sharingType: String,
  importance: Number,
  link: String,
  notes: String,
  lastCheck: Date,
  nextCheck: Date
});

module.exports = mongoose.model('Source', SourceSchema);
