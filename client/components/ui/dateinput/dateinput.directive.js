'use strict';

angular.module('eventsApp')
  .directive('dateinput', function () {
    var controller = function($scope) {
      $scope.open = function() {
        $scope.opened = true;
      };

      if (typeof $scope.format === 'undefined') {
        $scope.format = 'yyyy/MM/dd';
      }
      if (typeof $scope.min === 'undefined') {
        $scope.min = null;
      }
      if (typeof $scope.max === 'undefined') {
        $scope.max = null;
      }
      if (typeof $scope.required === 'undefined') {
        $scope.required = false;
      }
      if (typeof $scope.dateDisabled === 'undefined') {
        $scope.dateDisabled = false;
      }
      $scope.opened = false;
    };
    return {
      controller: controller,
      templateUrl: 'components/ui/dateinput/dateinput.html',
      scope: {
        format: '@',
        ngModel: '=',
        min: '@',
        max: '@',
        dateDisabled: '@',
        required: '@'
      }
    };
  });
