'use strict';

angular.module('eventsApp')
  .directive('keyTrap', ['$parse', function($parse) {
    return function( scope, element, attr ) {
      var inVals = attr.keyTrap.split(':');
      var fn = $parse(inVals[1]);
      var handler = function(event) {
        scope.$apply(function() {
          fn(scope, {$event:event});
        });
      };
      element.bind('keydown', function(event) {
        if(event.keyCode === inVals[0]) {
          event.preventDefault();
          handler(event);
        }
      });
    };
  }]);
