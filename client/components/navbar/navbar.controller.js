'use strict';

angular.module('eventsApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth, $rootScope) {
    $scope.menu = [{
      'title': 'By Date',
      'state': 'main.list'
    }, {
      'title': 'By Category',
      'state': 'main.list'
    }];
    $scope.adminMenu = [{
      'title': 'Sub-Categories',
      'state': 'subcats.list'
    }, {
      'title': 'Questions',
      'state': 'questions.list'
    }, {
      'title': 'Events',
      'state': 'events.list'
    }, {
      'title': 'Sources',
      'state': 'sources.list'
    }, {
      'title': 'Users',
      'state': 'admin'
    }];

    $scope.isCollapsed = true;
    $scope.isDropped = false;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $rootScope.$on('$stateChangeSuccess',
    function(event, toState, toParams, fromState, fromParams){
      $scope.isDropped = false;
    })
  });
