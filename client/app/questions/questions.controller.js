'use strict';

angular.module('eventsApp')
  .controller('QuestionsCtrl', function ($scope, $location, $state, lodash, Category, Question, Subcat) {
    $scope.Category = Category;
    $scope.categories = Category.items;
    $scope.questionTypes = [
      { val: 'single-select', label: 'Single-Select'},
      { val: 'multi-select', label: 'Multi-Select'},
      { val: 'text', label: 'Text'}
    ];

    $scope.questionTypeName = function(val) {
      return lodash.find($scope.questionTypes,function(rw){ return rw.val === val; });
    };

    $scope.create = function() {
      var question = new Question({
        name: this.question.name,
        questionType: this.question.questionType,
        answers: this.question.answers
      });

      question.$save(function (response) {
        $location.path('questions/' + response._id);
      });

      this.question.name = '';
    };

    $scope.find = function(query) {
      Question.query(query, function (questions) {
        $scope.questions = questions;
      });
    };

    $scope.newItem = function() {
      this.question = {};
    };

    $scope.findOne = function() {
      Question.get({ questionId: $state.params.questionId }, function (question) {
        $scope.question = question;
        Subcat.query({ questions: $state.params.questionId }, function (subcats) {
          $scope.question.subcats = [];
          angular.forEach(subcats, function(value) {
            $scope.question.subcats.push(value._id);
          });
        });
        Subcat.query({}, function (subcats) {
          $scope.subcatsByCategory = {};
          for (var i=0; i < subcats.length; i++) {
            if (typeof($scope.subcatsByCategory[subcats[i].category]) === 'undefined') {
              $scope.subcatsByCategory[subcats[i].category] = [];
            }
            $scope.subcatsByCategory[subcats[i].category].push(subcats[i]);
          }
        });
      });
    };

    $scope.update = function() {
      var question = $scope.question;
      question.$update(function() {
        $location.path('questions/' + question._id);
      });
    };

    $scope.destroy = function(question) {
      question.$remove();
      for (var i in $scope.questions) {
        if ($scope.questions[i] === question) {
          $scope.questions.splice(i, 1);
        }
      }
    };

    $scope.removeAnswer = function(i) {
      $scope.question.answers.splice(i, 1);
    };

    $scope.addAnswer = function() {
      if ($scope.question.answers === undefined) {
        $scope.question.answers = [{}];
      } else {
        $scope.question.answers.push({});
      }
    };

    $scope.removeTag = function(ans, i) {
      ans.tags.splice(i, 1);
    };

    $scope.addTag = function(ans) {
      if (ans.newTag !== undefined &&
          ans.newTag.length > 0 &&
          !lodash.contains(ans.tags, ans.newTag)) {
        if (ans.tags === undefined) {
          ans.tags = [ans.newTag];
        } else {
          ans.tags.push(ans.newTag);
        }
      }
      ans.newTag = '';
    };
  });
