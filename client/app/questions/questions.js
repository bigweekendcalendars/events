'use strict';

angular.module('eventsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('questions', {
        abstract: true,
        url: '/questions',
        templateUrl: 'app/questions/questions.html',
        controller: 'QuestionsCtrl',
        authenticate: true
      })
      .state('questions.list', {
        url: '',
        templateUrl: 'app/questions/questions.list.html',
        authenticate: true
      })
      .state('questions.create', {
        url: '/create',
        templateUrl: 'app/questions/questions.create.html',
        authenticate: true
      })
      .state('questions.detail', {
        url: '/:questionId',
        templateUrl: 'app/questions/questions.detail.html',
        authenticate: true
      })
      .state('questions.edit', {
        url: '/:questionId/edit',
        templateUrl: 'app/questions/questions.edit.html',
        authenticate: true
      })
  });
