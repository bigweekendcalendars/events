'use strict';

angular.module('eventsApp')
  .factory('Question', ['$resource', function($resource) {
    return $resource('/api/questions/:questionId', {
        questionId: '@_id',
      }, {
        update: { method: 'PUT' }
      }
    );
  }]);
