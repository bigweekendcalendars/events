'use strict';

describe('Controller: SourcesCtrl', function () {

  // load the controller's module
  beforeEach(module('eventsApp'));

  var SourcesCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SourcesCtrl = $controller('SourcesCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
