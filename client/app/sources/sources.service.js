'use strict';

angular.module('eventsApp')
  .factory('Source', ['$resource', function($resource) {
    return $resource('/api/sources/:sourceId', {
        sourceId: '@_id',
      }, {
        update: { method: 'PUT' }
      }
    );
  }]);
