'use strict';

angular.module('eventsApp')
  .controller('SourcesCtrl', function ($scope, $location, $state, lodash, Source, Event, $modal) {
    $scope.create = function() {
      var source = new Source(this.source);

      source.$save(function (response) {
        $location.path('sources/' + response._id);
      });

      this.source.name = '';
    };

    $scope.find = function(query) {
      Source.query(query, function (sources) {
        $scope.sources = sources;
      });
    };

    $scope.newItem = function() {
      this.source = {};
    };

    $scope.findOne = function() {
      Source.get({ sourceId: $state.params.sourceId }, function (source) {
        $scope.source = source;
      });
    };

    $scope.update = function() {
      var source = $scope.source;
      source.$update(function() {
        $location.path('sources/' + source._id);
      });
    };

    $scope.destroy = function(source) {
      source.$remove();
      for (var i in $scope.sources) {
        if ($scope.sources[i] === source) {
          $scope.sources.splice(i, 1);
        }
      }
    };

    $scope.loadEvents = function(source) {
      Event.query({ source: source._id }, function (events) {
        $scope.events = events;
        $scope.selectedSource = source;
      });
    };

    $scope.edit = function (sourceId) {
      Source.get({ sourceId: sourceId }, function (source) {
        $scope.source = source;
        var modalInstance = $modal.open({
          animation: false,
          templateUrl: 'app/sources/sources.modal.html',
          controller: 'SourceModalInstanceCtrl',
          size: 'lg',
          resolve: {
            title: function () {
              return 'New Source';
            },
            source: function () {
              return $scope.source;
            }
          }
        });
        modalInstance.result.then(function (source) {
          $scope.source = source;
          $scope.source.$update(function() {
            for (var i=0; i < $scope.sources.length; i++) {
              if ($scope.sources[i]._id === source._id) {
                $scope.sources[i] = source;
              }
            }
          });
        }, function () {});
      });
    };

    $scope.new = function () {
      $scope.source = {};
      var modalInstance = $modal.open({
        animation: false,
        templateUrl: 'app/sources/sources.modal.html',
        controller: 'SourceModalInstanceCtrl',
        size: 'lg',
        resolve: {
          title: function () {
            return 'Editing ' + $scope.source.name;
          },
          source: function () {
            return $scope.source;
          }
        }
      });
      modalInstance.result.then(function (source) {
        var newSource = new Source(source);
        newSource.$save(function (response) {
          $scope.sources.push(response);
        });
      }, function () {});
    };

    $scope.editEvent = function (eventId) {
      Event.get({ eventId: eventId }, function (event) {
        $scope.event = event;
        var modalInstance = $modal.open({
          animation: false,
          templateUrl: 'app/events/events.modal.html',
          controller: 'EventModalInstanceCtrl',
          size: 'lg',
          resolve: {
            title: function () {
              return 'Editing ' + $scope.event.name;
            },
            event: function () {
              return $scope.event;
            }
          }
        });
        modalInstance.result.then(function (event) {
          $scope.event = event;
          $scope.event.$update(function() {
            for (var i=0; i < $scope.events.length; i++) {
              if ($scope.events[i]._id === event._id) {
                $scope.events[i] = event;
              }
            }
          });
        }, function () {});
      });
    };
  })
  .controller('SourceModalInstanceCtrl', function ($scope, $modalInstance, source, title) {
    $scope.source = source;
    $scope.title = title;

    $scope.ok = function () {
      $modalInstance.close($scope.source);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  })
  .controller('EventModalInstanceCtrl', function ($scope, $modalInstance, event, title) {
    $scope.event = event;
    $scope.title = title;

    $scope.ok = function () {
      $modalInstance.close($scope.event);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  });
