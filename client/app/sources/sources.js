'use strict';

angular.module('eventsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('sources', {
        abstract: true,
        url: '/sources',
        templateUrl: 'app/sources/sources.html',
        controller: 'SourcesCtrl',
        authenticate: true
      })
      .state('sources.list', {
        url: '',
        templateUrl: 'app/sources/sources.list.html',
        authenticate: true
      });
  });
