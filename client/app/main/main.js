'use strict';

angular.module('eventsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        abstract: true,
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'EventsCtrl',
        authenticate: true
      })
      .state('main.list', {
        url: '',
        templateUrl: 'app/main/main.list.html',
        authenticate: true
      })
      .state('main.detail', {
        url: 'event/{eventId}',
        templateUrl: 'app/main/main.detail.html',
        controller: 'EventsCtrl',
        authenticate: true
      });
  });
