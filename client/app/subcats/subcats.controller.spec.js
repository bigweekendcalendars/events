'use strict';

describe('Controller: SubcatsCtrl', function () {

  // load the controller's module
  beforeEach(module('eventsApp'));

  var SubcatsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SubcatsCtrl = $controller('SubcatsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
