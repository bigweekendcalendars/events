'use strict';

angular.module('eventsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('subcats', {
        abstract: true,
        url: '/subcats',
        templateUrl: 'app/subcats/subcats.html',
        controller: 'SubcatsCtrl',
        authenticate: true
      })
      .state('subcats.list', {
        url: '',
        templateUrl: 'app/subcats/subcats.list.html',
        authenticate: true
      })
      .state('subcats.create', {
        url: '/create',
        templateUrl: 'app/subcats/subcats.create.html',
        authenticate: true
      })
      .state('subcats.detail', {
        url: '/{subcatId}',
        templateUrl: 'app/subcats/subcats.detail.html',
        authenticate: true
      })
      .state('subcats.edit', {
        url: '/{subcatId}/edit',
        templateUrl: 'app/subcats/subcats.edit.html',
        authenticate: true
      });
  });
