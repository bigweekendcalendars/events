'use strict';

angular.module('eventsApp')
  .factory('Subcat', ['$resource', function($resource) {
    return $resource('/api/subcats/:subcatId', {
        subcatId: '@_id',
      }, {
        update: { method: 'PUT' }
      }
    );
  }]);
