'use strict';

angular.module('eventsApp')
  .controller('SubcatsCtrl', function ($scope, $location, $state, lodash, Category, Subcat, Question) {
    $scope.Category = Category;
    $scope.categories = Category.items;

    $scope.create = function() {
      var subcat = new Subcat({
        name: this.subcat.name,
        category: this.subcat.category,
        code: this.subcat.code,
        tags: this.subcat.tags,
        questions: this.subcat.questions
      });

      subcat.$save(function (response) {
        $location.path('subcats/' + response._id);
      });

      this.subcat.name = '';
    };

    $scope.find = function(query) {
      Subcat.query(query, function (subcats) {
        $scope.subcats = subcats;
        $scope.subcatsByCategory = {};
        for (var i=0; i < subcats.length; i++) {
          if (typeof($scope.subcatsByCategory[subcats[i].category]) === 'undefined') {
            $scope.subcatsByCategory[subcats[i].category] = [];
          }
          $scope.subcatsByCategory[subcats[i].category].push(subcats[i]);
        }
      });
    };

    $scope.newItem = function() {
      this.subcat = {};
    };

    $scope.findOne = function() {
      Subcat.get({ subcatId: $state.params.subcatId }, function (subcat) {
        $scope.subcat = subcat;
        $scope.subcat.questionNames = [];
        angular.forEach(subcat.questions, function(qid) {
          Question.get({ questionId: qid }, function(question) {
            $scope.subcat.questionNames.push(question.name);
          });
        });
        Question.query({}, function (questions) {
          $scope.questions = questions;
        });
      });
    };

    $scope.update = function() {
      var subcat = $scope.subcat;
      subcat.$update(function() {
        $location.path('subcats/' + subcat._id);
      });
    };

    $scope.destroy = function(subcat) {
      subcat.$remove();
      for (var i in $scope.subcats) {
        if ($scope.subcats[i] === subcat) {
          $scope.subcats.splice(i, 1);
        }
      }
    };

    $scope.removeTag = function(i) {
      $scope.subcat.tags.splice(i, 1);
    };

    $scope.addTag = function() {
      if ($scope.subcat.newTag !== null &&
          $scope.subcat.newTag.length > 0 &&
            !lodash.contains($scope.subcat.tags, $scope.subcat.newTag)) {
        if ($scope.subcat.tags === undefined) {
          $scope.subcat.tags = [$scope.subcat.newTag];
        } else {
          $scope.subcat.tags.push($scope.subcat.newTag);
        }
        $scope.subcat.newTag = '';
      }
    };
  });
