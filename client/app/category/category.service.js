'use strict';

angular.module('eventsApp')
  .factory('Category', function(lodash) {
    var CATEGORY = {};
    CATEGORY.items = [
      { id: '1', name: 'Sports and Hobbies (and interests?)' },
      { id: '2', name: 'Music' },
      { id: '3', name: 'Observing Comedy, Theater and Performing Arts' },
      { id: '4', name: 'Observing Movies, TV and Internet' },
      { id: '5', name: 'Observing Visual Art and Design' },
      { id: '6', name: 'Eating and Drinking' },
      { id: '7', name: 'Animals' },
      { id: '8', name: 'Volunteering, Benefits, Causes (We need people to help our cause / I want to help)' },
      { id: '9', name: 'Themed, Holiday, Ethnic and Religious' },
      { id: '10', name: 'Professional and Personal Skills' },
      { id: '11', name: 'Books, Writing and Words' },
      { id: '12', name: 'Health resources (We offer a health resource / I need health help)' },
      { id: '13', name: 'Shopping' },
      { id: '14', name: 'Public Calls for Input' }
    ];
    CATEGORY.findById = function(id) {
      return lodash.find(CATEGORY.items,function(rw){ return rw.id === id; });
    };
    return CATEGORY;
  });
