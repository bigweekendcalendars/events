'use strict';

angular.module('eventsApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('events', {
        abstract: true,
        url: '/events',
        templateUrl: 'app/events/events.html',
        controller: 'EventsCtrl',
        authenticate: true
      })
      .state('events.list', {
        url: '',
        templateUrl: 'app/events/events.list.html',
        authenticate: true
      })
      .state('events.create', {
        url: '/create',
        templateUrl: 'app/events/events.create.html',
        authenticate: true
      })
      .state('events.detail', {
        url: '/{eventId}',
        templateUrl: 'app/events/events.detail.html',
        authenticate: true
      })
      .state('events.edit', {
        url: '/{eventId}/edit',
        templateUrl: 'app/events/events.edit.html',
        authenticate: true
      });
  });
