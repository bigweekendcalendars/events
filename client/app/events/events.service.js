'use strict';

angular.module('eventsApp')
  .factory('Event', ['$resource', function($resource) {
    return $resource('/api/events/:eventId', {
        eventId: '@_id',
      }, {
        update: { method: 'PUT' }
      }
    );
  }]);
