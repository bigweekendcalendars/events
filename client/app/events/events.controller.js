'use strict';

angular.module('eventsApp')
  .controller('EventsCtrl', function ($scope, $location, $state, Category, Subcat, Question, Source, Event, lodash, Auth) {
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.Category = Category;
    $scope.categories = Category.items;
    $scope.showCategories = true;
    $scope.showSubcats = false;
    $scope.searching = false;

    $scope.create = function() {
      var fields = [ 'name', 'title', 'description', 'nickname', 'website', 'date', 'location', 'subcats',
        'weights', 'answers', 'cost', 'attendance', 'pets', 'kids', 'age', 'feel', 'rsvp', 'name', 'category',
        'code', 'tags', 'questions' ],
        newEventAttrs = {};
      for (var i=0; i < fields.length; i++) {
        newEventAttrs[fields[i]] = this.event[fields[i]];
      }
      var event = new Event(newEventAttrs);

      event.$save(function (response) {
        $location.path('events/' + response._id);
      });

      this.event.name = '';
    };

    $scope.find = function(query) {
      if (typeof query === 'undefined' && typeof $scope.search !== 'undefined') {
        $scope.searching = true;
        query = { s: $scope.search };
      }
      Event.query(query, function (events) {
        $scope.events = events;
      });
    };

    var initScope = function() {
      $scope.cats = [null,null,null];
      $scope.subcats = [null,null,null];
      $scope.questions = [null,null,null];
      $scope.has_questions = [false,false,false];
    };

    var initEvent = function() {
      if ($scope.event.place.length < 1) {
        $scope.event.place.push({});
      }
      for (var i=0; i < $scope.event.place.length; i++) {
        if ($scope.event.place[i] !== null) {
          $scope.event.place[i].date = Date.parse($scope.event.place[i].date);
          $scope.event.place[i].start = Date.parse($scope.event.place[i].start);
          $scope.event.place[i].end = Date.parse($scope.event.place[i].end);
        }
      }
      while ($scope.event.subcats.length < 3) {
        $scope.event.subcats.push(null);
      }
      if ($scope.event.weights.length < 3) {
        $scope.event.weights.push(0);
      }
      if ($scope.event.answers === null || typeof($scope.event.answers) === 'undefined') {
        $scope.event.answers = {};
      }
      for (i=0; i < $scope.event.subcats.length; i++) {
        if ($scope.event.subcats[i] !== null) {
          setCategory($scope.event.subcats[i], i);
        }
      }
      getSources();
    };

    $scope.newItem = function() {
      $scope.event = {
        place: [{ location: null, date: null, start: null, end: null }],
        subcats: [null,null,null],
        weights: [0,0,0],
        answers: {},
        tags: [],
        cost: { min: null, max: null },
        feel: { fun: null, classy: null },
        rsvp: { required: false, start: null, end: null },
      };
      initScope();
    };

    var setCategory = function(subcatId, i) {
      Subcat.get({ subcatId: subcatId }, function (subcat) {
        $scope.cats[i] = subcat.category;
      });
    };
    $scope.findOne = function(id) {
      if (id === null) {
        id = $state.params.eventId;
      }
      initScope();
      Event.get({ eventId: id }, function (event) {
        $scope.event = event;
        initEvent();
      });
    };

    $scope.update = function() {
      var event = $scope.event;
      event.$update(function() {
        $location.path('events/' + event._id);
      });
    };

    $scope.destroy = function(event) {
      event.$remove();
      for (var i in $scope.events) {
        if ($scope.events[i] === event) {
          $scope.events.splice(i, 1);
        }
      }
    };

    var getSources = function() {
      Source.query({}, function (sources) {
        $scope.sources = sources;
      });
    };

    var getSubcats = function(i, catQuery) {
      Subcat.query({ category: catQuery }, function (subcats) {
        $scope.subcats[i] = subcats;
      });
    };
    $scope.$watchCollection('cats', function(newValue) {
      if (newValue !== null && typeof newValue !== 'undefined') {
        for (var i=0; i < newValue.length; i++) {
          if (newValue[i] !== null && typeof(newValue[i]) !== 'undefined') {
            getSubcats(i, newValue[i]);
          }
        }
      }
    }, true);
    var getQuestions = function(i, subcatId) {
      Subcat.get({ subcatId: subcatId }, function (subcat) {
        $scope.questions[i] = {};
        $scope.has_questions[i] = subcat.questions.length > 0;
        for (var j=0; j < subcat.questions.length; j++) {
          Question.get({ questionId: subcat.questions[j] }, function (question) {
            $scope.questions[i][question._id] = question;
          });
        }
      });
    };
    $scope.$watchCollection('event.subcats', function(newValue) {
      if (newValue !== null && typeof(newValue) !== 'undefined') {
        for (var i=0; i < newValue.length; i++) {
          if (newValue[i] !== null && typeof(newValue[i]) !== 'undefined') {
            getQuestions(i, newValue[i]);
          }
        }
      }
    }, true);

    $scope.addPlace = function() {
      var lastPlace = null;
      if ($scope.event.place.length > 0) {
        lastPlace = $scope.event.place[$scope.event.place.length - 1];
      }
      if (lastPlace !== null) {
        $scope.event.place.push({ location: lastPlace.location, date: lastPlace.date, start: lastPlace.start, end: lastPlace.end });
      } else {
        $scope.event.place.push({});
      }
    };
    $scope.removePlace = function(i) {
      $scope.event.place.splice(i, 1);
    };

    $scope.selectCat = function(catId) {
      Subcat.query({ category: catId }, function (subcats) {
        $scope.subcats = subcats;
        $scope.showCategories = false;
        $scope.showSubcats = true;
      });
    };

    $scope.selectSubcat = function(subcatId) {
      Event.query({ subcats: subcatId }, function (events) {
        $scope.events = events;
        $scope.showSubcats = false;
      });
    };

    $scope.clearSearch = function() {
        $scope.searching = false;
        $scope.events = [];
    };

    $scope.clearCat = function() {
        $scope.showCategories = true;
        $scope.showSubcats = false;
        $scope.events = [];
    };

    $scope.clearSubcat = function() {
        $scope.showCategories = false;
        $scope.showSubcats = true;
        $scope.events = [];
    };

    $scope.selectedCategoryName = function(i) {
      if ($scope.cats[i] === null || typeof $scope.cats[i] === 'undefined') {
        return '';
      }
      return $scope.categories[$scope.cats[i]].name;
    };

    $scope.selectedSubcatName = function(i) {
      if ($scope.event === null || typeof $scope.event === 'undefined') {
        return '';
      }
      if ($scope.event.subcats[i] === null || typeof $scope.event.subcats[i] === 'undefined') {
        return '';
      }
      var selected = lodash.filter($scope.subcats[i], function(item) {
        if (item._id === $scope.event.subcats[i]) {
          return true;
        }
        return false;
      });
      if (selected.length < 1) {
        return '';
      }
      return selected[0].name;
    };
  });
